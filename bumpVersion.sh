#!/bin/bash

pushd "$(dirname "$0")"

CURRENT_VERSION=`cat .version`
CURRENT_MAJOR=`echo $CURRENT_VERSION | cut -d. -f1`
CURRENT_MINOR=`echo $CURRENT_VERSION | cut -d. -f2`
CURRENT_PATCH=`echo $CURRENT_VERSION | cut -d. -f3`

NEW_MINOR=$((CURRENT_MINOR+1))
NEW_VERSION=$CURRENT_MAJOR.$NEW_MINOR.0

echo "$CURRENT_VERSION --> $NEW_VERSION"

echo -n $NEW_VERSION > .version

popd
