#!/bin/bash
pushd `dirname $0` > /dev/null

TOKEN=$(./sonarqube_token.sh)

echo "SONAR_TOKEN=${TOKEN}" > ../.sonar_token.mk

popd > /dev/null
