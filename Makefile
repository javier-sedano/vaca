ifeq ($(VERSION),)
	VERSION := dev
endif

dependenciesInit:
	composer install
	cd front && npm run dependenciesInit && cd ..

clean:
	rm -rf var
	rm -rf gen
	rm -f .env.*.php
	rm -rf .scannerwork
	rm -rf coverage
	rm -rf public/vaca/web
	cd front && npm run clean && cd ..

deepClean: clean
	rm -rf bin/.phpunit
	rm -rf vendor
	rm -rf front/node_modules

devStart:
	./parallel.sh "make buildFrontWatch" "make symfonyStart"

symfonyStart:
	install -d var/log/
	touch var/log/dev.log
	install -d ~/.symfony/autoupdate/
	touch ~/.symfony/autoupdate/silence
	symfony server:start --no-tls --port=4003 --allow-all-ip

gen-version:
	install -d gen/
	echo $(VERSION) > gen/version.txt

gen-env-prod:
	composer dump-env prod

build: clean testFront test buildFront gen-version gen-env-prod

test:
	XDEBUG_MODE=coverage php ./bin/phpunit --coverage-clover coverage/coverage.xml --coverage-text --coverage-html coverage/html/

testFront:
	cd front && npm run test -- run && cd ..

testFrontWatch:
	cd front && npm run test && cd ..

buildFront:
	cd front && npm run build-only -- --outDir=../public/vaca/web/ && cd ..

buildFrontWatch:
	cd front && npm run build-only -- --mode=development --watch --emptyOutDir --sourcemap=true --minify=false --outDir=../public/vaca/web/ && cd ..

-include .sonar_token.mk
sonar:
	sonar-scanner -Dsonar.host.url=http://sonarqube:9000 -Dsonar.token=$(SONAR_TOKEN)
	@echo "If dockerized, browse http://localhost:9003"
	rm -rf .scannerwork

sonarqube-start:
	./.devcontainer/sonarqube_start.sh

sonarqube-stop:
	 ./.devcontainer/sonarqube_stop.sh

test-n-sonar:
	make test
	make testFront
	make sonarqube-start
	make sonar

auditSymfony:
	symfony check:security

auditFront:
	cd front && npm audit && cd ..

auditFrontWithIgnores:
	cd front && npm run auditWithIgnores && cd ..
