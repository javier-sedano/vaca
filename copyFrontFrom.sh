#!/bin/bash

declare -a IGNORE_DIRS=(
    "/node_modules"
    "/dist"
    "/coverage"
    "/gen"
)

declare -a SKIP_FILES=(
    "/src/assets/app.png"
    "/public/favicon.png"
    "/public/css/theme.css"
)

declare -a REPLACE_FILES=(
    "/index.html"
    "/vite.config.ts"
    "/src/__tests__/tests.utils.ts"
    "/src/services/service.constants.ts"
)

declare -a REPLACE_EXTENSIONS=(
    ".snap"
)



FROM="$1"
[ -z "$FROM" ] && {
    echo "Usage: copyFrontFrom <fromDir_relative>"
    echo "Example: copyFrontFrom ../vaca"
    exit -1
}
FROM=`echo $FROM | sed "s|/*$||"`
FROM_FRONT=../$FROM/front

pushd "$(dirname "$0")"

FROM_NAME=$(basename $FROM)
FROM_CAMEL_NAME=${FROM_NAME^}
TO_NAME=$(basename `pwd`)
TO_CAMEL_NAME=${TO_NAME^}

cd front

echo ""
echo "  Removing files/dirs in TO that does not exist in FROM..."
find | while read TO_FILE
do
  for IGNORE in "${IGNORE_DIRS[@]}"
  do
    IGNORE_PATH=.$IGNORE
    if [[ $TO_FILE == ${IGNORE_PATH}* ]]
    then
      echo "Ignored TO $TO_FILE because it matches $IGNORE from IGNORE_DIRS"
      continue
    fi
  done
  FROM_FILE=$FROM_FRONT/$TO_FILE
  if [ -f "$FROM_FILE" -o -d "$FROM_FILE" ]
  then
    continue
  fi
  echo "Removing TO $TO_FILE because it does not exist in FROM"
  rm -rf $TO_FILE
done

echo ""
echo "  Copying files/dirs in FROM to TO..."
find $FROM_FRONT | while read FROM_FILE
do
  for IGNORE in "${IGNORE_DIRS[@]}"
  do
    IGNORE_PATH=$FROM_FRONT$IGNORE
    if [[ $FROM_FILE == ${IGNORE_PATH}* ]]
    then
      echo "Ignored FROM $FROM_FILE because it matches $IGNORE from IGNORE_DIRS"
      continue 2
    fi
  done
  for SKIP in "${SKIP_FILES[@]}"
  do
    SKIP_PATH=$FROM_FRONT$SKIP
    if [[ $FROM_FILE == ${SKIP_PATH} ]]
    then
      echo "Skipped FROM $FROM_FILE because it matches $SKIP from SKIP_FILES"
      continue 2
    fi
  done
  TO_FILE=`echo $FROM_FILE | sed "s|$FROM_FRONT/||"`

  for REPLACE in "${REPLACE_FILES[@]}"
  do
    REPLACE_PATH=$FROM_FRONT$REPLACE
    if [[ $FROM_FILE == ${REPLACE_PATH} ]]
    then
      echo "Replacing FROM $FROM_FILE TO $TO_FILE because it matches $REPLACE from REPLACE_FILES"
      cat $FROM_FILE | sed s/$FROM_NAME/$TO_NAME/g | sed s/$FROM_CAMEL_NAME/$TO_CAMEL_NAME/g > $TO_FILE
      continue 2
    fi
  done
  for REPLACE_EXTENSION in "${REPLACE_EXTENSIONS[@]}"
  do
    if [[ $FROM_FILE == *${REPLACE_EXTENSION} ]]
    then
      echo "Replacing FROM $FROM_FILE TO $TO_FILE because it matches $REPLACE from REPLACE_EXTENSIONS"
      cat $FROM_FILE | sed s/$FROM_NAME/$TO_NAME/g | sed s/$FROM_CAMEL_NAME/$TO_CAMEL_NAME/g > $TO_FILE
      continue 2
    fi
  done
  if [ -d $FROM_FILE ]
  then
    echo "Creating directory FROM $FROM_FILE TO $TO_FILE"
    install -d $TO_FILE
  fi
  if [ -f $FROM_FILE ]
  then
    echo "Coping from FROM $FROM_FILE TO $TO_FILE"
    cp $FROM_FILE $TO_FILE
  fi

done

popd
