Reference architecture of public web application with mobile-first responsive design based on PHP/Symfony and Vue with CI in Gitlab-CI and CD to Google Compute Engine.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/vaca/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/vaca/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.vaca&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.vaca)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
</details>

Keywords: php, symfony, phpunit, jest, sonarqube, vue, vscode, devcontainer, docker, yargs, nodemon, concurrently, w3css, fontawesome, giphy

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* Makefile targets will be shown in Task Explorer
  * Run "dependenciesInit" to download dependencies (command line: make dependenciesInit)
  * Run "devStart" to start serving (while watching for changes in both frontend and backend) (command line: make devStart)
    * Most common development tasks are configured as Makefile targets
  * Browse http://localhost:4003/

Useful links:

* https://code.visualstudio.com/docs/remote/containers
* https://code.visualstudio.com/docs/editor/tasks
* https://www.gnu.org/software/make/manual/make.html
* https://www.php.net/
* https://symfony.com/
* https://phpunit.de/
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://jestjs.io/
* https://github.com/axios/axios
* https://www.w3schools.com/w3css/
* https://fontawesome.com/
* https://developers.giphy.com/docs/api/endpoint/#random
