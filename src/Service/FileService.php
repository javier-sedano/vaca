<?php

namespace App\Service;

class FileService
{
    public function fileGetContents($file): string|false
    {
        return @\file_get_contents($file);
    }
}
