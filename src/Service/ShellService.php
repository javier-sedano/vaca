<?php

namespace App\Service;

class ShellService
{
    public function shellExec(string $command): string|false|null
    {
        return \shell_exec($command);
    }
}
