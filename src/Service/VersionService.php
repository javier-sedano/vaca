<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

class VersionService
{
    const VERSION_FILE = __DIR__ . '/../../gen/version.txt';

    private $logger;
    private $fileService;

    public function __construct(LoggerInterface $logger, FileService $fileService)
    {
        $this->logger = $logger;
        $this->fileService = $fileService;
    }

    public function getVersion(): string
    {
        $this->logger->debug("In the development environment the version file does not exist");
        $version = $this->fileService->fileGetContents(VersionService::VERSION_FILE);
        if ($version === false) {
            return "undefined";
        } else {
            return $version;
        }
    }
}
