<?php

namespace App\Service;

use App\Service\ShellException;
use Psr\Log\LoggerInterface;

class FortuneService
{
    const MAX_FORTUNES = 5;
    const FORTUNE = '/usr/games/fortune';

    private $logger;
    private $shellService;

    public function __construct(LoggerInterface $logger, ShellService $shellService)
    {
        $this->logger = $logger;
        $this->shellService = $shellService;
    }

    public function getFortunes($n): array
    {
        $this->logger->debug("Generating $n fortunes");
        if ($n <= 0) {
            throw new \RangeException("$n is too small");
        }
        if ($n > FortuneService::MAX_FORTUNES) {
            throw new \RangeException("$n is too large");
        }
        $fortunes = array();
        for ($i = 0; $i < $n; $i++) {
            $fortune = $this->shellService->shellExec(FortuneService::FORTUNE);
            if ($fortune == null) {
                throw new ShellException("Unable to run " . FortuneService::FORTUNE);
            }
            array_push($fortunes, $fortune);
        }
        return $fortunes;
    }
}
