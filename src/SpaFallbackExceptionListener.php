<?php
namespace App;

use App\Controller\RestConstants;
use App\Service\FileService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SpaFallbackExceptionListener
{
    const SPA_INDEX = "../public" . RestConstants::BASE_WEB_URL . "index.html";

    private $logger;
    private $fileService;

    public function __construct(LoggerInterface $logger, FileService $fileService)
    {
        $this->logger = $logger;
        $this->fileService = $fileService;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof NotFoundHttpException) {
            $request = $event->getRequest();
            $pathInfo = $request->getPathInfo();
            if ($this->startsWith($pathInfo, RestConstants::BASE_WEB_URL)) {
                $this->logger
                    ->debug("Probably $pathInfo is SPA route, trying " . SpaFallbackExceptionListener::SPA_INDEX);
                $event->allowCustomResponseCode();
                $response = new Response(
                    $this->fileService->fileGetContents(SpaFallbackExceptionListener::SPA_INDEX),
                    Response::HTTP_OK,
                    ['content-type' => 'text/html']
                );
                $event->setResponse($response);
            }
        }
    }

    private function startsWith($haystack, $needle): bool
    {
        $length = strlen($needle);
        return substr($haystack, 0, $length) === $needle;
    }
}
