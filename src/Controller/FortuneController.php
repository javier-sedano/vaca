<?php

namespace App\Controller;

use App\Service\FortuneService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class FortuneController extends AbstractController
{
    const URL = RestConstants::BASE_REST_URL . "fortune";

    private $fortuneService;

    public function __construct(FortuneService $fortuneService)
    {
        $this->fortuneService = $fortuneService;
    }

    #[Route(FortuneController::URL, name: 'fortune')]
    public function fortune(Request $request): JsonResponse
    {
        $n = $request->query->get('n', 1);
        $fortunes = $this->fortuneService->getFortunes($n);
        return new JsonResponse($fortunes);
    }
}
