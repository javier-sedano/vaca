<?php

namespace App\Controller;

use App\Service\VersionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class VersionController extends AbstractController
{
    const URL = RestConstants::BASE_REST_URL . "version";

    private $versionService;

    public function __construct(VersionService $versionService)
    {
        $this->versionService = $versionService;
    }

    #[Route(VersionController::URL, name: 'version')]
    public function version(): Response
    {
        return new Response(
            $this->versionService->getVersion(),
            Response::HTTP_OK,
            ['content-type' => 'text/plain']
        );
    }
}
