<?php
namespace App\Controller;

class RestConstants
{
    const ROOT_URL = '/';
    const BASE_URL = RestConstants::ROOT_URL . 'vaca/';
    const BASE_REST_URL = RestConstants::BASE_URL . 'rest/';
    const BASE_WEB_URL = RestConstants::BASE_URL . 'web/';
}
