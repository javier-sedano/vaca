export interface OracleAnswer {
  answer: string;
  gifUrl: string;
  giphyUrl: string;
  author?: string;
}
