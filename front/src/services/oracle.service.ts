import axios from "axios";
import { type OracleAnswer } from "./OracleAnswer";

const giphyRandom = "https://api.giphy.com/v1/gifs/random";
const giphyApiKey = "74g5KeZbM2ZlREjWYLxEPOlPayx0xH4J";
const maxRelationFactor = 3;
const minRelationFactor = 1 / maxRelationFactor;

const answers = [
  "Yes",
  "No",
  "Maybe",
];

export const oracleService = {
  getResponse: async (requestedRatio: number): Promise<OracleAnswer> => {
    const answerNumber = Math.floor(Math.random() * answers.length);
    const answer = answers[answerNumber];
    const response = await axios.get(giphyRandom, {
      params: {
        api_key: giphyApiKey,
        tag: answer,
        limit: 1,
      },
    });
    const data = response.data.data;
    const gotRatio = data.images.original.height / data.images.original.width;
    const relation = requestedRatio / gotRatio;
    if (relation > maxRelationFactor || relation < minRelationFactor) {
      console.log("Discarded gif with aspect ration relation " + relation);
      return await oracleService.getResponse(requestedRatio);
    } else {
      console.log("Choose gif with aspect ratio relation " + relation);
      return {
        answer,
        gifUrl: data.images.original.url,
        giphyUrl: data.url,
        author: data.user?.display_name,
      };
    }
  },
};
