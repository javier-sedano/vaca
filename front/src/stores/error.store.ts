import { defineStore } from "pinia";
import { ref } from "vue";

export const useErrorStore = defineStore("error", () => {
  const errorMessage = ref<string | null>(null);

  function setErrorMessage(newErrormessage: string | null) {
    errorMessage.value = newErrormessage;
  }

  function clearErrorMessage() {
    setErrorMessage(null);
  }

  return {
    errorMessage,
    setErrorMessage,
    clearErrorMessage,
  };
});
