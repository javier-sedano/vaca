import { describe, it, expect } from "vitest";
import { shallowMount } from "@vue/test-utils";
import NotFound from "../../../views/NotFound.vue";

describe("NotFound.vue", async () => {
  it("Should render empty", async () => {
    const wrapper = shallowMount(NotFound);
    expect(wrapper.element).toMatchSnapshot("Initial");
  });
});
