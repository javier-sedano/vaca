import { vi, type MockedFunction } from "vitest";
import axios from "axios";

export const REST_BASE_URL = "/vaca/rest";

export function mockAxios(): {
  axiosGetMock: MockedFunction<typeof axios.get>;
  axiosPostMock: MockedFunction<typeof axios.post>;
  axiosPutMock: MockedFunction<typeof axios.put>;
  axiosDeleteMock: MockedFunction<typeof axios.delete>;
} {
  vi.mock("axios", () => {
    return {
      default: {
        get: vi.fn(),
        post: vi.fn(),
        put: vi.fn(),
        delete: vi.fn(),
      },
    };
  });
  const axiosGetMock = axios.get as MockedFunction<typeof axios.get>;
  const axiosPostMock = axios.post as MockedFunction<typeof axios.post>;
  const axiosPutMock = axios.put as MockedFunction<typeof axios.put>;
  const axiosDeleteMock = axios.delete as MockedFunction<typeof axios.delete>;
  return {
    axiosGetMock,
    axiosPostMock,
    axiosPutMock,
    axiosDeleteMock,
  };
}
