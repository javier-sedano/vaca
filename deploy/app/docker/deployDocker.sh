#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/vaca/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=vaca" --filter "status=running" --quiet`
docker container stop vaca
docker container rm vaca
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 96m -p 5380:80 --restart unless-stopped --name vaca $IMAGE
if [ "$containerId" ]
then
  docker container start vaca
fi
