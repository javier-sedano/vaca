<?php

namespace App\Tests\Integration\Service;

use App\Service\VersionService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VersionServiceIntegrationTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testGetVersionOK(): void
    {
        $versionService = static::getContainer()->get(VersionService::class);
        $version = $versionService->getVersion();
        $this->assertEquals("undefined", $version);
    }
}
