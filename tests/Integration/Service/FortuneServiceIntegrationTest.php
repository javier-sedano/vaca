<?php

namespace App\Tests\Integration\Service;

use App\Service\FortuneService;
use App\Tests\TestConstants;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FortuneServiceIntegrationTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $kernel = self::bootKernel();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testGetOneFortune()
    {
        $fortuneService = static::getContainer()->get(FortuneService::class);
        $fortunes = $fortuneService->getFortunes(1);
        $this->assertEquals(1, count($fortunes));
    }

    public function testGetSeveralFortunes()
    {
        $fortuneService = static::getContainer()->get(FortuneService::class);
        $fortunes = $fortuneService->getFortunes(TestConstants::MAX_FORTUNES);
        $this->assertEquals(TestConstants::MAX_FORTUNES, count($fortunes));
    }

    public function testGetTooManyFortunes()
    {
        $this->expectException(\RangeException::class);
        $fortuneService = static::getContainer()->get(FortuneService::class);
        $fortunes = $fortuneService->getFortunes(TestConstants::MAX_FORTUNES + 1);
        $this->fail();
    }

    public function testGetTooFewFortunes()
    {
        $this->expectException(\RangeException::class);
        $fortuneService = static::getContainer()->get(FortuneService::class);
        $fortunes = $fortuneService->getFortunes(0);
        $this->fail();
    }
}
