<?php

namespace App\Tests\Unit\Service;

use App\Service\FortuneService;
use App\Service\ShellService;
use App\Service\ShellException;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class FortuneServiceUnitTest extends TestCase
{
    public function testAFortune()
    {
        $aFortune = "e=mc2";
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $shellService = $this->createMock(ShellService::class);
        $shellService->method('shellExec')->willReturn($aFortune);
        $fortuneService = new FortuneService($loggerInterface, $shellService);
        $fortunes = $fortuneService->getFortunes(1);
        $this->assertEquals(1, count($fortunes));
        $this->assertEquals($aFortune, $fortunes[0]);
    }

    public function testShellError()
    {
        $this->expectException(ShellException::class);
        $aFortune = "e=mc2";
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $shellService = $this->createMock(ShellService::class);
        $shellService->method('shellExec')->willReturn(null);
        $fortuneService = new FortuneService($loggerInterface, $shellService);
        $fortunes = $fortuneService->getFortunes(1);
        $this->fail();
    }

}
