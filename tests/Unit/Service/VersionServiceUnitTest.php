<?php

namespace App\Tests\Unit\Service;

use App\Service\VersionService;
use App\Service\FileService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class VersionServiceUnitTest extends TestCase
{
    public function testAVersion()
    {
        $aVersion = "123";
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $fileService = $this->createMock(FileService::class);
        $fileService->method('fileGetContents')->willReturn($aVersion);
        $versionService = new VersionService($loggerInterface, $fileService);
        $version = $versionService->getVersion();
        $this->assertEquals($aVersion, $version);
    }

    public function testNoVersionFile()
    {
        $loggerInterface = $this->createMock(LoggerInterface::class);
        $fileService = $this->createMock(FileService::class);
        $fileService->method('fileGetContents')->willReturn(false);
        $versionService = new VersionService($loggerInterface, $fileService);
        $version = $versionService->getVersion();
        $this->assertEquals("undefined", $version);
    }
}
