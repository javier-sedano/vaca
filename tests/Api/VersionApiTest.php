<?php

namespace App\Tests\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VersionApiTest extends WebTestCase
{
    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetVersion()
    {
        $this->client->request(
            'GET',
            ApiTestConstants::BASE_REST_URL . 'version'
        );
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("text/plain; charset=UTF-8", $response->headers->get('Content-Type'));
        $this->assertEquals("undefined", $response->getContent());
    }
}
