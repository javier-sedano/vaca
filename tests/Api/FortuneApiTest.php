<?php

namespace App\Tests\Api;

use App\Tests\TestConstants;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FortuneApiTest extends WebTestCase
{
    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetOneFortune()
    {
        $this->client->request(
            'GET',
            ApiTestConstants::BASE_REST_URL . 'fortune'
        );
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("application/json", $response->headers->get('Content-Type'));
        $responseData = json_decode($response->getContent());
        $this->assertEquals(1, count($responseData));
    }

    public function testGetSeveralFortunes()
    {
        $this->client->request(
            'GET',
            ApiTestConstants::BASE_REST_URL . 'fortune',
            ['n' => TestConstants::MAX_FORTUNES]
        );
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals("application/json", $response->headers->get('Content-Type'));
        $responseData = json_decode($response->getContent());
        $this->assertEquals(TestConstants::MAX_FORTUNES, count($responseData));
    }

    public function testGetTooManyFortunes()
    {
        $this->client->request(
            'GET',
            ApiTestConstants::BASE_REST_URL . 'fortune',
            ['n' => TestConstants::MAX_FORTUNES + 1]
        );
        $response = $this->client->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    public function testGetZeroFortunes()
    {
        $this->client->request(
            'GET',
            ApiTestConstants::BASE_REST_URL . 'fortune',
            ['n' => 0]
        );
        $response = $this->client->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }
}
