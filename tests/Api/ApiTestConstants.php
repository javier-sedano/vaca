<?php
namespace App\Tests\Api;

class ApiTestConstants
{
    const ROOT_URL = '/';
    const BASE_URL = ApiTestConstants::ROOT_URL . 'vaca/';
    const BASE_REST_URL = ApiTestConstants::BASE_URL . 'rest/';
    const BASE_WEB_URL = ApiTestConstants::BASE_URL . 'web/';
}
