<?php

namespace App\Tests\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RootApiTest extends WebTestCase
{
    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testRootRedirection()
    {
        $this->_testRedirection(ApiTestConstants::ROOT_URL, ApiTestConstants::BASE_URL);
    }

    public function testBaseRedirection()
    {
        $this->_testRedirection(ApiTestConstants::BASE_URL, ApiTestConstants::BASE_WEB_URL);
    }

    private function _testRedirection($url, $dest)
    {
        $this->client->request(
            'GET',
            $url
        );
        $response = $this->client->getResponse();
        $this->assertEquals(301, $response->getStatusCode());
        $location = $response->headers->get("Location");
        $this->assertEquals("http://localhost$dest", $location);
    }
}
